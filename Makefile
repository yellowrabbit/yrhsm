MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

# To build lib by nonstandard compiler set CC and AR env variables:
# make CC=..... AR=.... lib

SHELL       := zsh
.ONESHELL:
# error on unset vars, exit on error in pipe commands 
.SHELLFLAGS := -o nounset -e -c
.DELETE_ON_ERROR:

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

# dirs
OUTPUT              := .out
LIBDIR              := .out


# Flags
CFLAGS          := -pipe -O2 -g -std=gnu11 -fdiagnostics-color=always \
                   -fno-math-errno -fno-printf-return-value \
                   -ffunction-sections -fdata-sections -fno-inline \
                   ${MISC_FLAGS}


CINC_APP        := -I./include
CFLAGS_APP      := ${CFLAGS} ${CINC_APP}

CINC_LIB        := -I./include
CFLAGS_LIB      := ${CFLAGS} ${CINC_LIB}

LDFLAGS         :=  -L${LIBDIR}

WARNINGS        := -Wall -Wextra -Werror \
                   -Wformat-overflow=2 -Wshift-overflow=2 -Wimplicit-fallthrough=5 \
                   -Wformat-signedness -Wformat-truncation=2 \
                   -Wstringop-overflow=4 -Wunused-const-variable=2 -Walloca \
                   -Warray-bounds=2 -Wswitch-bool -Wsizeof-array-argument \
                   -Wduplicated-branches -Wduplicated-cond \
                   -Wlto-type-mismatch -Wnull-dereference \
                   -Wdangling-else \
                   -Wpacked -Wfloat-equal -Winit-self -Wmissing-include-dirs \
                   -Wmissing-noreturn -Wbool-compare \
                   -Wsuggest-attribute=noreturn -Wsuggest-attribute=format \
                   -Wmissing-format-attribute \
                   -Wuninitialized -Wtrampolines -Wframe-larger-than=2048 \
                   -Wunsafe-loop-optimizations -Wshadow -Wpointer-arith -Wbad-function-cast \
                   -Wcast-qual -Wwrite-strings -Wsequence-point -Wlogical-op \
                   -Wlogical-not-parentheses \
                   -Wredundant-decls -Wvla -Wdisabled-optimization \
                   -Wunreachable-code -Wparentheses -Wdiscarded-array-qualifiers \
                   -Wmissing-prototypes -Wold-style-definition -Wold-style-declaration \
                   -Wmissing-declarations \
                   -Wcast-align -Winline -Wmultistatement-macros -Warray-bounds=2 \
                   \
                   -Wno-error=cast-qual \
                   -Wno-error=unsafe-loop-optimizations \
                   \
                   -Wno-packed \
                   -Wno-unused-parameter \

#####################################
# The project parts
YRHSM_DIR           := .
YRHSM_SRC           := ${YRHSM_DIR}/src
YRHSM_INCLUDE       := ${YRHSM_DIR}/include
YRHSM_OUTPUT        := ${YRHSM_DIR}/${OUTPUT}
YRHSM_SENTINEL      := ${YRHSM_OUTPUT}/.sentinel
YRHSM_LIB           := yrhsm
YRHSM_LIB_FILE      := ${LIBDIR}/lib${YRHSM_LIB}.a

TEST_DIR            := test
TEST_ELF            := test
TEST_SRC            := ${TEST_DIR}/src
TEST_INCLUDE        := ${TEST_DIR}/include
TEST_OUTPUT         := ${TEST_DIR}/${OUTPUT}
TEST_ELF_FILE       := ${TEST_OUTPUT}/${TEST_ELF}
TEST_SENTINEL       := ${TEST_OUTPUT}/.sentinel
TEST_LINKMAP_FILE   := ${TEST_OUTPUT}/linkmap.txt
TEST_CFLAGS         := ${CFLAGS}
TEST_LDFLAGS        := -Wl,--gc-sections -Wl,--cref -Wl,-Map=${TEST_LINKMAP_FILE}

#####################################
# Batch comiple. The parameters are:
# - the output path
# - list of C files  
# - compiler options
#
# zsh/gnu-make magic to make the obj file name: {name%pattern} 
# removes the pattern from the name, use $$ in order to pass the $ to zsh
# -o $(@D)/$${$$(basename $${src})%.c}.o
define compile_c_files
    mkdir -p $(1)
    for src in $(2); do
        ${CC} ${WARNINGS} $(3) \
            -c $${src} -o $(1)/$${$$(basename $${src})%.c}.o ;
    done
endef

#####################################
#  Compile with options for external libs
#  The parameters:
#  - the output path
#  - list of C files
#  - inc flags
define compile_ext_lib
    $(call compile_c_files, $(1), $(2), ${CFLAGS_LIB} $(3))
endef

#####################################
#  Compile with options for the app files
#  including local libs 
#  The parameters:
#  - the output path
#  - list of C files
#  - inc flags
define compile_app
    $(call compile_c_files, $(1), $(2), ${CFLAGS_APP} $(3))
endef

#####################################
all: test
.PHONY: all

lib: yrhsm
.PHONY: lib

clean:
>   find `pwd` -name ${OUTPUT} -exec rm -rf \{\} +

#####################################
# Yellow Rabbit HSM library
# := static assigment
# = dynamic assigment
YRHSM_C_FILES       := $(shell find ${YRHSM_SRC} -name '*.c')
YRHSM_H_DEPS        := $(shell find ${YRHSM_INCLUDE} -name '*.h')
YRHSM_O_FILES       = $(shell find ${YRHSM_OUTPUT} -name '*.o')
YRHSM_INC_CFLAGS    := -I${YRHSM_INCLUDE}

${YRHSM_SENTINEL}: ${YRHSM_C_FILES} ${YRHSM_H_DEPS}
>   $(call compile_ext_lib, $(@D), ${YRHSM_C_FILES}, ${YRHSM_INC_CFLAGS})
>   touch $@

${YRHSM_LIB_FILE}: ${YRHSM_SENTINEL}
>   mkdir -p ${LIBDIR}  
>   rm -rf $@ 2> /dev/null
>   ${AR} r $@ ${YRHSM_O_FILES}

yrhsm:   ${YRHSM_LIB_FILE}
.PHONY: yrhsm

#####################################
# It's lib, so there is no main app but test suite
# --start-group/--end-group make linker
#  search for names several times in the
#  group
# := static assigment
# = dynamic assigment
TEST_C_FILES    := $(shell find ${TEST_SRC} -name '*.c')
TEST_H_DEPS     := $(shell find ${TEST_INCLUDE} -name '*.h') \
                   $(shell find ${YRHSM_INCLUDE} -name '*.h') 
TEST_O_FILES    = $(shell find ${TEST_OUTPUT} -name '*.o')
TEST_INC_CFLAGS := -I${TEST_INCLUDE} -I${YRHSM_INCLUDE}

${TEST_SENTINEL}: ${TEST_C_FILES} ${TEST_H_DEPS}
>   $(call compile_app, $(@D), ${TEST_C_FILES}, ${TEST_INC_CFLAGS})
>   touch $@

${TEST_ELF_FILE}: ${TEST_SENTINEL} yrhsm
>   mkdir -p ${TEST_OUTPUT}
>   ${CC} ${TEST_CFLAGS} ${TEST_LDFLAGS} \
    -Wl,--start-group \
    ${TEST_O_FILES} ${YRHSM_LIB_FILE} \
    -Wl,--end-group -o $@

test:   ${TEST_ELF_FILE}
.PHONY: test

# vim: expandtab: ts=4 sw=4 ft=yrmake:
