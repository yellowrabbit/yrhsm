#ifndef EVENT_H
#define EVENT_H

#include <stdint.h>

#ifdef __DragonFly__
    #define ALIGN4 __attribute__((aligned(4)))
    #define RODATA
#else
    // esp8266 :)
    #include "8266-utils.h"
#endif

/****************************************
 * Minimal event
 * id
 * flags - unused for now (0)
 ****************************************/
typedef uint8_t  event_id_t;
typedef uint8_t  event_flags_t;

typedef struct {
    event_id_t          id ALIGN4;
    event_flags_t       flags;
} event_t ;

/****************************************
 * Helper macros for events
 ****************************************/
#define DECL_EVENT_TYPE(_machine, _ev, ...) \
    typedef struct { \
        event_t super __attribute__ ((aligned (4))); \
        __VA_ARGS__ \
        } _machine##_ev##_t

#define DECL_SIMPLE_EVENT(_machine, _ev, _var) \
    extern _machine##SIMPLE_t const _var

#define DECL_EVENT(_machine, _ev, _var) \
    extern _machine##_ev##_t _var

#define DEF_EVENT(_machine, _ev, _var, ...) \
    _machine##_ev##_t _var = { \
        .super.id = _ev, .super.flags = 0, \
        ##__VA_ARGS__ }

#define DEF_SIMPLE_EVENT(_machine, _ev, _var) \
    _machine##SIMPLE_t const RODATA _var = { \
        .super.id = _ev, .super.flags = 0 }

// event type casting
#define TYPE_E_CONST(_machine, _ev, _var) \
    ((_machine##_ev##_t const *)_var)
#define TYPE_E(_machine, _ev, _var) \
    ((_machine##_ev##_t *)_var)

// use in event handlers (standard ev variable)
#define THIS_E_CONST(_machine, _ev) \
    TYPE_E_CONST(_machine, _ev, ev)
#define THIS_E(_machine, _ev) \
    TYPE_E(_machine, _ev, ev)

/****************************************
 * First user event starts with
 * USER_FIRST_EV
 ****************************************/
enum EVENT_ID_ENUM {
    ENTRY_EV = 0,
    EXIT_EV,
    DEFAULT_EV,         // default auto transition
    DONT_DELEGATE_EV,   // events above this can't travel through hierarchy
    HSM_MACHINE_INIT,   // reinitialize all HS machine by
                        // exit from and entry to top state
    USER_FIRST_EV
};

/****************************************
 * Global static events
 ****************************************/
extern event_t const gl_hsm_machine_init_ev;
extern event_t const gl_entry_ev;
extern event_t const gl_exit_ev;
extern event_t const gl_default_ev;

#endif
// vim: set expandtab: sw=4 ts=4:
