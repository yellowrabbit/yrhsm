#ifndef ASSERT_H
#define ASSERT_H


/****************************************
 * OS-specific error reporting
 ****************************************/
#ifdef __DragonFly__
#include <stdio.h>
#include <stdlib.h>
#define __assert_func(_file, _line) \
    fprintf(stderr, "%s:%d assertion failed\n", \
            _file, _line)
#define assert(_e) if (!(_e)) { \
    __assert_func(__FILE__, __LINE__); \
    abort(); }
#else
    // esp8266 :)
#include <stddef.h>
#include "8266-utils.h"
#define __assert_func(_pc) \
    os_printf("Assertion failed at %lx\n", _pc)
#define assert(_e) if (!(_e)) { \
    uint32_t pc; \
    __asm__ __volatile__ ( \
                "call0 1f\n" \
                ".align 32\n" \
                "1:\n" \
                "mov.n %0, a0\n" \
                : "=&r" (pc) \
                : \
                : "a0" \
            ); \
    __assert_func(pc); \
    __asm__ __volatile__ ( "ill\n"); }
#endif // os_printf

#endif
// vim: set expandtab: sw=4 ts=4:
