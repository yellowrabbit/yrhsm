#ifndef HSM_H
#define HSM_H

#include <stddef.h>

#include "event.h"

// max ``levels'' of the HSM
#define MAX_HSM_DEPTH 10    // if >127 then fix trans.c
#define HSM_DEPTH_CNT_TYPE int8_t

/****************************************
 * Event handler result
 ****************************************/
typedef enum {
    UNHANDLED   = 0,
    HANDLED,
} handle_result_t;

/****************************************
 * Event handler func
 * state   - this state (not current machine state,
 * just handler's state)
 * machine - current machine
 * event   - event to handle
 ****************************************/
struct machine_s;
struct state_s;
typedef handle_result_t (*handle_func_t)(struct machine_s *, event_t const *);

/****************************************
 * The machine state
 ****************************************/
typedef struct state_s {
    // event handler
    // parent state (NULL for top)
    struct state_s const * const parent;
    // handler
    handle_func_t const handle;
} state_t;


/****************************************
 * Root state used for boot and init the machines
 ****************************************/
extern state_t hsm_boot_state;

/****************************************
 * The machine
 ****************************************/
typedef struct machine_s {
    // current state
    state_t const *cur_state;
    // init state
    state_t const *init_state;
} machine_t;

/****************************************
 * constructor
 * state -- init state
 ****************************************/
void hsm_create(machine_t *, state_t *);

// handle event
#define hsm_handle(_machine, _ev) \
    (((machine_t *)(_machine))->cur_state-> \
      handle((machine_t *)_machine, (event_t const*)_ev))

// call parent handler
#define hsm_parent_handle(_state, _machine, _ev) \
    ((((event_t const*)_ev)->id > DONT_DELEGATE_EV) ? \
     ((state_t const*)_state)->parent->handle( \
         ((machine_t *)_machine), ((event_t const*)_ev)) : HANDLED)

// initialization
#define hsm_init(_machine) \
    hsm_handle(_machine, &gl_hsm_machine_init_ev);

// transition from one state to another
// machine   - current machine
// act_state - state initiated transition
// to_state  - destination state
// needed in order to separate (D)S11->S1 and (A) S1->S1
void trans(machine_t *, state_t const*, state_t const*);

// DSL
// declare machine
// defines new type: machinename_t
#define DECL_MACHINE(_machine, ...) \
    typedef struct { \
        machine_t super; \
        __VA_ARGS__ \
        } _machine##_t; \
    void _machine##_create(_machine##_t *); \
    void _machine##_init(_machine##_t *); \
    DECL_EVENT_TYPE(_machine, SIMPLE); \
    handle_result_t _machine##_handle(_machine##_t *, event_t const *) \

#define MACHINE_TYPE(_machine) _machine##_t
#define MACHINE_TYPE_CAST(_machine, _v) ((_machine##_t)(_v))
#define MACHINE_TYPE_PTR(_machine) (_machine##_t *)
#define MACHINE_TYPE_PTR_CAST(_machine, _v) ((_machine##_t *)(_v))

#define DEF_MACHINE(_machine, _boot_state) \
    void _machine##_create(_machine##_t *machine) { \
        hsm_create(&machine->super, &_machine##_boot_state);

#define END_MACHINE }

// The machine methods
// MACHINE_CREATE(type-name, &inst)
#define MACHINE_CREATE(_machine, _var) \
    _machine##_create(_var)

// MACHINE_INIT(type-name, &inst)
#define MACHINE_INIT(_machine, _var) \
    hsm_init(((machine_t*)(_var)))

// declare state
// _machine - machine type name
// _state   - state name
#define DECL_STATE(_machine, _state) \
    handle_result_t _machine##_state##handle(machine_t *, event_t const*); \
    extern state_t _machine##_state;

// define state's handler
// _machine - machine type name
// _state   - state name
// _parent  - parent state
#define DEF_ROOT_STATE(_machine, _state, _parent) \
    state_t _machine##_state = {&hsm_boot_state, _machine##_state##handle}; \
    handle_result_t _machine##_state##handle( \
            machine_t *machine, event_t const*ev) {

#define DEF_STATE(_machine, _state, _parent) \
    state_t _machine##_state = {&_machine##_parent, _machine##_state##handle}; \
    handle_result_t _machine##_state##handle( \
            machine_t *machine, event_t const*ev) {

#define END_STATE(_machine, _state) \
    return(hsm_parent_handle(&_machine##_state, machine, ev)); }

/****************************************
 * Helper functions
 ****************************************/
// cast standard variable (machine) to the machine type pointer
#define THIS_M(_machine) \
        ((_machine##_t *) machine)

// transition
#define TRANS(_machine, _act_state, _to_state) \
    trans(machine, &_machine##_act_state, &_machine##_to_state);

// transitiona to history
#define TRANS_HISTORY(_machine, _act_state, _to_state_var) \
    trans(machine, &_machine##_act_state, (_to_state_var));

// get state addr
#define GET_STATE(_machine, _state) \
    &_machine##_state

#endif //HSM_H
// vim: expandtab: sw=4 ts=4:
