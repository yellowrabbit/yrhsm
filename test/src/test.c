#include <stdio.h>
#include <stdint.h>

#include "dbg.h"
#include "test-machine.h"

//static event_t const a_ev = { .id = A_EV, .flags = 0};
//static event_t const b_ev = { .id = B_EV, .flags = 0};

DEF_EVENT(test_machine, A_EV, const a_ev,
        .port = 123);
DEF_EVENT(test_machine, B_EV, b_ev,
        .flag = 321);

//static event_t const c_ev = { .id = C_EV, .flags = 0};
static event_t const d_ev = { .id = D_EV, .flags = 0};
static event_t const e_ev = { .id = E_EV, .flags = 0};
static event_t const f_ev = { .id = F_EV, .flags = 0};
static event_t const g_ev = { .id = G_EV, .flags = 0};
static event_t const h_ev = { .id = H_EV, .flags = 0};
static event_t const i_ev = { .id = I_EV, .flags = 0};

int main(int argc, char *argv[]) {
    MACHINE_TYPE(test_machine) machine;

    // internal init
    MACHINE_CREATE(test_machine, &machine);
    MACHINE_INIT(test_machine, &machine);

    DEF_EVENT(test_machine, C_EV, c_ev);

    dbg(" S211-A>");
    hsm_handle(&machine, (event_t const *)&a_ev);
    dbg(" S21-B>");
    hsm_handle(&machine, (event_t const *)&b_ev);
    dbg(" S2-C>");
    hsm_handle(&machine, (event_t const *)&c_ev);
    dbg(" S1-A>");
    hsm_handle(&machine, (event_t const *)&a_ev);
    dbg(" S1-B>");
    hsm_handle(&machine, (event_t const *)&b_ev);
    dbg(" S1-C>");
    hsm_handle(&machine, (event_t const *)&c_ev);
    dbg(" S211-D>");
    hsm_handle(&machine, &d_ev);
    dbg(" S-E>");
    hsm_handle(&machine, &e_ev);
    dbg(" S1-D>");
    hsm_handle(&machine, &d_ev);
    dbg(" S211-D>");
    hsm_handle(&machine, &d_ev);
    dbg(" S-E>");
    hsm_handle(&machine, &e_ev);
    dbg(" S1-F>");
    hsm_handle(&machine, &f_ev);
    dbg(" S2-F>");
    hsm_handle(&machine, &f_ev);
    dbg(" S11-G>");
    hsm_handle(&machine, &g_ev);
    dbg(" S21-G>");
    hsm_handle(&machine, &g_ev);
    dbg(" S11-H>");
    hsm_handle(&machine, &h_ev);
    dbg(" S1-I>");
    hsm_handle(&machine, &i_ev);
    dbg(" S1-C>");
    hsm_handle(&machine, (event_t const *)&c_ev);
    dbg(" S211-H>");
    hsm_handle(&machine, &h_ev);
    dbg(" S11-G>");
    hsm_handle(&machine, &g_ev);
    dbg(" S2-I>");
    hsm_handle(&machine, &i_ev);

    //dbg(" S1-C>");
    //hsm_handle(&machine, (event_t const *)&c_ev);
    dbg(" S211-boot>");
    hsm_handle(&machine, &gl_hsm_machine_init_ev);
    /*
    dbg(" S211-G>");
    hsm_handle(&machine, &g_ev);
    dbg(" S1-I>");
    hsm_handle(&machine, &i_ev);
    dbg(" S1-A>");
    hsm_handle(&machine, &a_ev);
    dbg(" S1-D>");
    hsm_handle(&machine, &d_ev);
    dbg(" S11-D>");
    hsm_handle(&machine, &d_ev);
    dbg(" S1-C>");
    hsm_handle(&machine, &c_ev);
    dbg(" S-E>");
    hsm_handle(&machine, &e_ev);
    dbg(" S-E>");
    hsm_handle(&machine, &e_ev);
    dbg(" S11-G>");
    hsm_handle(&machine, &g_ev);
    */

    return(machine.foo);
}
// vim: set expandtab: sw=4 ts=4:
