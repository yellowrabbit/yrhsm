#include <stddef.h>

#include <assert.h>
#include <dbg.h>

#include "test-machine.h"

/****************************************
 * States
 ****************************************/
DEF_ROOT_STATE(test_machine, S, NULL)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s-init-");
            TRANS(test_machine, S, S11);
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s-exit-");
            return(HANDLED);
        case E_EV:
            dbg(" -s-E:");
            TRANS(test_machine, S, S11);
            return(HANDLED);
        case I_EV:
            dbg(" -s-I:");
            if (THIS_M(test_machine)->foo) {
                THIS_M(test_machine)->foo = 0;
                return(HANDLED);
            }
            break;
    }
END_STATE(test_machine, S)

DEF_STATE(test_machine, S1, S)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s1-init-");
            TRANS(test_machine, S1, S11);
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s1-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s1-exit-");
            return(HANDLED);
        case A_EV:
            dbg(" -s1-A:");
            TRANS(test_machine, S1, S1);
            return(HANDLED);
        case B_EV:
            dbg(" -s1-B:");
            TRANS(test_machine, S1, S11);
            return(HANDLED);
        case C_EV:
            dbg(" -s1-C: <%d>", THIS_E_CONST(test_machine, C_EV)->flag);
            TRANS(test_machine, S1, S2);
            return(HANDLED);
        case D_EV:
            dbg(" -s1-D:");
            if (!THIS_M(test_machine)->foo) {
                THIS_M(test_machine)->foo = 1;
                TRANS(test_machine, S1, S);
                return(HANDLED);
            }
            break;
        case F_EV:
            dbg(" -s1-F:");
            TRANS(test_machine, S1, S211);
            return(HANDLED);
    }
END_STATE(test_machine, S1)

DEF_STATE(test_machine, S11, S1)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s11-init-");
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s11-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s11-exit-");
            return(HANDLED);
        case D_EV:
            dbg(" -s11-D:");
            if (THIS_M(test_machine)->foo) {
                THIS_M(test_machine)->foo = 0;
                TRANS(test_machine, S11, S1);
                return(HANDLED);
            }
            break;
        case G_EV:
            dbg(" -s11-G:");
            TRANS(test_machine, S11, S211);
            return(HANDLED);
        case H_EV:
            dbg(" -s11-H:");
            TRANS(test_machine, S11, S);
            return(HANDLED);
        case I_EV:
            dbg(" -s11-I:");
            return(HANDLED);
    }
END_STATE(test_machine, S11)

DEF_STATE(test_machine, S2, S)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s2-init-");
            TRANS(test_machine, S2, S211);
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s2-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s2-exit-");
            return(HANDLED);
        case C_EV:
            dbg(" -s2-C:");
            TRANS(test_machine, S2, S1);
            return(HANDLED);
        case F_EV:
            dbg(" -s2-F:");
            TRANS(test_machine, S2, S11);
            return(HANDLED);
        case I_EV:
            dbg(" -s2-I:");
            if (!THIS_M(test_machine)->foo) {
                THIS_M(test_machine)->foo = 1;
                return(HANDLED);
            }
            break;
    }
END_STATE(test_machine, S2)

DEF_STATE(test_machine, S21, S2)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s21-init-");
            TRANS(test_machine, S21, S211);
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s21-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s21-exit-");
            return(HANDLED);
        case A_EV:
            dbg(" -s21-A:");
            TRANS(test_machine, S21, S21);
            return(HANDLED);
        case B_EV:
            dbg(" -s21-B:");
            TRANS(test_machine, S21, S211);
            return(HANDLED);
        case G_EV:
            dbg(" -s21-G:");
            TRANS(test_machine, S21, S1);
            return(HANDLED);
    }
END_STATE(test_machine, S21)

DEF_STATE(test_machine, S211, S21)
    switch (ev->id) {
        case DEFAULT_EV:
            dbg(" -s211-init-");
            return(HANDLED);
        case ENTRY_EV:
            dbg(" -s211-entry-");
            return(HANDLED);
        case EXIT_EV:
            dbg(" -s211-exit-");
            return(HANDLED);
        case D_EV:
            dbg(" -s211-D:");
            TRANS(test_machine, S211, S21);
            return(HANDLED);
        case H_EV:
            dbg(" -s211-H:");
            TRANS(test_machine, S211, S);
            return(HANDLED);
    }
END_STATE(test_machine, S211)

// constructor
DEF_MACHINE(test_machine, S2)
    machine->foo = 0;
END_MACHINE

// vim: expandtab: sw=4 ts=4:
