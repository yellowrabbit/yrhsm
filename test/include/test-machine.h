// test machine
// https://i.hizliresim.com/kMPrjJ.png
#ifndef TEST_MACHINE_H
#define TEST_MACHINE_H

#include <stdbool.h>

#include "hsm.h"

DECL_MACHINE(test_machine,
        bool foo;);

/****************************************
 * States
 * machine, state
 ****************************************/
DECL_STATE(test_machine, S);
    DECL_STATE(test_machine, S1);
        DECL_STATE(test_machine, S11);
    DECL_STATE(test_machine, S2);
        DECL_STATE(test_machine, S21);
            DECL_STATE(test_machine, S211);

/****************************************
 * Events
 ****************************************/
enum {
    A_EV = USER_FIRST_EV,
    B_EV, C_EV, D_EV, E_EV, F_EV, G_EV, H_EV, I_EV,
};

DECL_EVENT_TYPE(test_machine, A_EV,
        int port;);
DECL_EVENT(test_machine, A_EV, const a_ev);

DECL_EVENT_TYPE(test_machine, B_EV,
        int flag;);
DECL_EVENT(test_machine, B_EV, b_ev);

DECL_EVENT_TYPE(test_machine, C_EV,
        char *name;
        uint8_t flag;);

#endif//TEST_MACHINE_H

// vim: expandtab: sw=4 ts=4:
