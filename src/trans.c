/****************************************
 * Transition from one state to another.
 * Big ugly function, the heart of the HSM
 ****************************************/
#include "assert.h"
#include "dbg.h"

#include "hsm.h"

// in order to discover the hsm hierarchy I need
// a small temp buffer for states
static state_t const *rev_states[MAX_HSM_DEPTH]; // from dest state to common
static HSM_DEPTH_CNT_TYPE rev_states_cnt;


/****************************************
 * Transition to state and default handle
 ****************************************/
static void default_trans(machine_t *, state_t const*);
static void default_trans(machine_t *machine, state_t const *to_state) {
    // actually transition the state
    machine->cur_state = to_state;
    // swith to default if have one
    to_state->handle(machine, &gl_default_ev);
}

/****************************************
 * case 0. From child to parent.
 ****************************************/
static void child_to_parent_trans(machine_t *, state_t const*, state_t const*,
        state_t const*);
static void child_to_parent_trans(machine_t *machine,
        state_t const *from_state,
        state_t const *to_state,
        state_t const *act_state) {
    const state_t *state = from_state;

    while(state != to_state) {
        state->handle(machine, &gl_exit_ev);
        state = state->parent;
    }
    // delegated event S1->S1
    if (to_state == act_state) {
        act_state->handle(machine, &gl_exit_ev);
        act_state->handle(machine, &gl_entry_ev);
    }
}

/****************************************
 * case 1. From parent to child
 ****************************************/
static void parent_to_child_trans(machine_t *, state_t const*);
static void parent_to_child_trans(machine_t *machine, state_t const *to_state) {
    for(HSM_DEPTH_CNT_TYPE i = rev_states_cnt - 1; i >= 0; --i) {
        rev_states[i]->handle(machine, &gl_entry_ev);
    }
}

/****************************************
 * case 2. Complex transition
 ****************************************/
static void complex_trans(machine_t *, HSM_DEPTH_CNT_TYPE const,
        state_t const*, state_t const*);
static void complex_trans(machine_t *machine, HSM_DEPTH_CNT_TYPE const parent,
        const state_t *src_state, const state_t *to_state) {
    // exits
    const state_t *common_parent = rev_states[parent];
    const state_t *state = src_state;
    do {
        state->handle(machine, &gl_exit_ev);
        state = state->parent;
    } while(state != common_parent);

    // entries
    for(HSM_DEPTH_CNT_TYPE i = parent - 1; i >= 0; --i) {
        rev_states[i]->handle(machine, &gl_entry_ev);
    }
}

/****************************************
 * There are 3 cases of transition:
 * 0) from child state Sy to parent state S3,
 * each state remembers its parent so search for
 * destination is quick.
 * There is a catch: one need to diff between
 * delegated and local events.
 * FROM: Sy
 *        |
 *        v
 *       S33
 *        |
 *        v
 * TO:   S3
 * 1) from parent state S1 to child state S11,
 * Collects all parent states in the temp array.
 * TO:   S11
 *        ^
 *        |
 *       Sxx
 *        ^
 *        |
 * FROM: S1
 * 2) most complex case: from S22 through
 * common parent Sx to S11.
 * Uses temp array of the parent states from case 1
 * TO:   S11   S22
 *        ^     |
 *        |     v
 *       S1 <-- S2
 *           Sx
 * Boot of the machine is special variant of the case 1,
 * it's easy detected by the from state == NULL
 ****************************************/
void trans(machine_t *me, state_t const *act_state, state_t const *to_state) {
    assert(me);
    assert(to_state);
    state_t const *state;
    state_t const *src_state;

    // if we call from the parent handler of the current state
    // then transit to the parent first
    src_state = me->cur_state;
    if (src_state != act_state) {
        dbg("[-1]");
        child_to_parent_trans(me, src_state, act_state, NULL);
        src_state = act_state;
    }
    // see if it's the case 0
    state = src_state;
    while (state && state != to_state) {
        state = state->parent;
    }
    if (state) { // case 0
        dbg("[0] ");
        child_to_parent_trans(me, src_state, to_state, act_state);
    } else { // nah, not the case 0
        // collect parents
        state = to_state;
        rev_states_cnt = 0;
        while (state && state != src_state) {
            rev_states[rev_states_cnt++] = state;
            state = state->parent;
        }
        assert(rev_states_cnt < MAX_HSM_DEPTH);
        if (state) { // case 1
            dbg("[1] ");
            parent_to_child_trans(me, to_state);
        } else { // case 2
            dbg("[2] ");
            state = src_state;
            assert(state->parent);
            do {
                state = state->parent;
                for (HSM_DEPTH_CNT_TYPE i = 1; i < rev_states_cnt; ++i) {
                    if (rev_states[i] == state) {
                        complex_trans(me, i, src_state, to_state);
                        state = NULL;
                        break;
                    }
                }
            } while(state);
        }
    }

    // Actual transition
    dbg("[<=go] ");
    default_trans(me, to_state);
}
// vim: expandtab: sw=4 ts=4:
