/****************************************
 * The machine functions
 ****************************************/
#include "assert.h"
#include "dbg.h"

#include "hsm.h"

/****************************************
 * Boot state func.
 ****************************************/
static handle_result_t hsm_boot_handle(machine_t *, event_t const*);
static handle_result_t hsm_boot_handle(machine_t *machine, event_t const *ev) {
    switch (ev->id) {
        case HSM_MACHINE_INIT:
            dbg(" boot ");
            trans(machine, &hsm_boot_state, machine->init_state);
            return(HANDLED);
    }
    return(HANDLED);
}

state_t hsm_boot_state RODATA = {.parent = NULL, .handle = hsm_boot_handle};

// constructor
void hsm_create(machine_t *me, state_t *init_state) {
    dbg("hsm_create ");
    assert(me);
    assert(init_state);
    // remember state for use in the machine reinitialization
    me->init_state = init_state;
    me->cur_state = &hsm_boot_state;
}

// vim: expandtab: sw=4 ts=4:
