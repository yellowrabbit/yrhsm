/****************************************
 * const events
 ****************************************/
#include "event.h"

event_t const RODATA gl_hsm_machine_init_ev =
                        {.id = HSM_MACHINE_INIT, .flags = 0};
event_t const RODATA gl_entry_ev   = {.id = ENTRY_EV, .flags = 0};
event_t const RODATA gl_exit_ev    = {.id = EXIT_EV, .flags = 0};
event_t const RODATA gl_default_ev       = {.id = DEFAULT_EV, .flags = 0};

// vim: expandtab: sw=4 ts=4:
